package com.binus.Model;

import org.junit.Test;

import static org.junit.Assert.*;

public class TaskTest {
    @Test
    public void givenAnObjectWithValues_whenValuesAsked_thenReturnValues(){
        String expectedName = "Testing";
        String expectedStatus = "DONE";
        int expectedId = 1;

        Task task = new Task();
        task.setId(expectedId);
        task.setName(expectedName);
        task.setStatus(expectedStatus);

        assertEquals(expectedId, task.getId());
        assertEquals(expectedName, task.getName());
        assertEquals(expectedStatus, task.getStatus());
    }

    @Test
    public void giveAnObjectCreatedUsingConstructor_whenTheValuesAsked_returnTheAskedValues(){
        String expectedName = "Testing";
        String expectedStatus = "DONE";
        int expectedId = 1;

        Task task = new Task(expectedName, expectedStatus, expectedId);

        assertEquals(expectedId, task.getId());
        assertEquals(expectedName, task.getName());
        assertEquals(expectedStatus, task.getStatus());
    }
}