package com.binus.Model;

import org.junit.Test;

import static org.junit.Assert.*;

public class TodoListTest {

    @Test
    public void whenGivenEmptyObject_whenTotalTaskAsked_returnTotalTask(){
        int expectedOutput = 0;
        TodoList todolist = new TodoList();

        int todoListLength = todolist.getTotalTask();

        assertEquals(expectedOutput, todoListLength);
    }

    @Test
    public void whenGivenObjectWithTasks_whenTotalTaskAsked_returnTotalTask(){
        int expectedOutput = 3;
        TodoList todolist = new TodoList();
        todolist.add("Testing 1","DONE");
        todolist.add("Testing 2","NOT DONE");
        todolist.add("Testing 3","DONE");

        int todoListLength = todolist.getTotalTask();

        assertEquals(expectedOutput, todoListLength);
    }


    @Test
    public void whenGivenObjectWithValues_whenAllTasksString_returnAllTaskString(){
        String expectedOutput = "1. Testing 1 [DONE]\n2. Testing 2 [NOT DONE]\n3. Testing 3 [DONE]\n";
        TodoList todolist = new TodoList();
        todolist.add("Testing 1","DONE");
        todolist.add("Testing 2","NOT DONE");
        todolist.add("Testing 3","DONE");

        String realOutput = todolist.getTodoListInString();

        assertEquals(expectedOutput, realOutput);
    }



}