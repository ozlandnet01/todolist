package com.binus.App;

import com.binus.Model.TodoList;

public class Main {
    public static void main(String[] args) {
        TodoList todoList = new TodoList();

        todoList.add("Do dishes", "DONE");
        todoList.add("Learn Java", "NOT DONE");
        todoList.add("Learn TDD", "DONE");

        System.out.println(todoList.getTodoListInString());
    }
}
