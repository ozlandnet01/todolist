package com.binus.Model;

import java.util.ArrayList;

public class TodoList {
    private ArrayList<Task> tasks;
    private int currId;

    public TodoList(){
        this.tasks = new ArrayList<Task>();
    }

    public void add(String name, String Status){
        addId();
        Task task = new Task(name, Status, getCurrId());
        this.tasks.add(task);
    }

    public int getTotalTask(){
        return this.tasks.size();
    }

    private int getCurrId(){
        return this.currId;
    }

    private void addId(){
        this.currId++;
    }

    public String getTodoListInString(){
        String todo = "";
        for(int i = 0 ; i < this.getTotalTask() ; i++){
            Task task = this.tasks.get(i);
            todo += task.getId() + ". " + task.getName() + " [" + task.getStatus() + "]\n";
        }
        return todo;
    }
}
