package com.binus.Model;



public class Task {
    private String name;
    private String status;
    private int id;

    public Task() {
    }

    public Task(String name, String status, int id) {
        this.name = name;
        this.status = status;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
